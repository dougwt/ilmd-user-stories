from django.db import models


class Role(models.Model):
    name = models.CharField(max_length=30)

    def __unicode__(self):
        return self.name


class Priority(models.Model):
    name = models.CharField(max_length=10)
    value = models.SmallIntegerField()

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'priorities'


class UserStory(models.Model):
    role = models.ForeignKey('Role')
    action = models.TextField(max_length=140)
    reason = models.TextField(max_length=140)
    priority = models.ForeignKey('Priority')
    date_added = models.DateTimeField(auto_now=False, auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True, auto_now_add=True)

    def __unicode__(self):
        return '[%s - %s] As a %s, I want to %s so that %s.' % (
            self.priority,
            self.id,
            self.role,
            self.action,
            self.reason
        )

    class Meta:
        verbose_name = 'User Story'
        verbose_name_plural = 'User Stories'
