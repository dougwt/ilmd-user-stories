from django.shortcuts import render

from .models import UserStory


def home(request):
    context = {'stories': UserStory.objects.all()}
    return render(request, 'stories/index.html', context)
