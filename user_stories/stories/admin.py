from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.contrib.auth.models import Group

from .models import Role, Priority, UserStory


# class PriorityInline(admin.TabularInline):
#     model = Priority
#     extra = 1


class PriorityAdmin(admin.ModelAdmin):
    # inlines = [PriorityInline]
    list_display = ('name', 'value')


admin.site.register(Role)
admin.site.register(Priority, PriorityAdmin)
admin.site.register(UserStory)

admin.site.unregister(User)
admin.site.unregister(Group)
admin.site.unregister(Site)
