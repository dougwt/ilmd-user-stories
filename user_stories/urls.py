from django.conf.urls import patterns, include, url
from django.contrib import admin

admin.autodiscover()
urlpatterns = patterns(
    '',
    url(r'^$', 'user_stories.stories.views.home', name='home'),
    # url(r'^user_stories/', include('user_stories.foo.urls')),

    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
