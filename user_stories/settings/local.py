# settings/local.py
from unipath import Path

from .base import *

DEBUG = True
TEMPLATE_DEBUG = DEBUG

# EMAIL_HOST = localhost
# EMAIL_PORT = 1025

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3',
#         'NAME': PROJECT_DIR.child("database", "user_stories.sqlite"),
#         'USER': '',
#         'PASSWORD': '',
#         'HOST': '',
#         'PORT': '',
#     }
# }

INTERNAL_IPS = ("127.0.0.1")
